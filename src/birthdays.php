<?php

use Compucie\Congressus\ExtendedClient;
use Compucie\Database\Member\MemberDatabaseManager;

require_once('../vendor/autoload.php');

header("Content-Type: application/json;");

$env = parse_ini_file("../.env", true);
$dbm = new MemberDatabaseManager($env['db-newtonapi-birthdays']);
$memberIds = $dbm->getMemberIdsWithBirthdayToday();

$congressusClient = new ExtendedClient($env['congressus']['token']);
$birthdayMembers = $congressusClient->retrieveMembers($memberIds);

$birthdays = array();

if (date("md") == "0325") {
	$birthdays[] = "ODMC";
} elseif (date("md") == "0506") {
	$birthdays[] = "Diepzat";
}

foreach ($birthdayMembers as $member) {
	$birthdays[] = "{$member->getFirstName()} {$member->getLastName()}";
}

echo json_encode($birthdays);
