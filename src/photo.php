<?php
//$root = "../photo/";.
$root = "/volume1/photo/";
$path = $root;
function folders_and_images_in_path($path, $root)
{
	$extension_allowed= array(
		'png',
		'jpg',
		'jpeg',
		'gif',
		'mp4'
	);
	$folders=[];
	$img=[];
	$items_in_path = scandir($path, 1);
	foreach ($items_in_path as $item){
		if ($item == '.' || $item == '..')
		{
			continue;
		}
		elseif (is_dir($path.$item))
		{
			$folders[]=$item;
		}
		elseif (is_file($path.$item))  
		{
			$file_extension=  strtolower (pathinfo($item, PATHINFO_EXTENSION));
			if(in_array($file_extension, $extension_allowed)){
				$abs_path = substr($path, strlen($root));
				$img[] =  $abs_path.$item;
			}
		}
	}
	return array($folders, $img);
}


if (isset($_GET['dir'])){
	if ($_GET['dir'] == 'banner'){
		$path = 'banner/current_banner/';
	}
	list($folders, $img)=folders_and_images_in_path($path, '');
}
elseif (isset($_GET['new'])){
	if ($_GET['new'] == intval ($_GET['new'])){
		$input=array();
		if (isset($_GET['last'])){
			$last = json_decode($_GET['last']);
			if (is_array($last)){
				foreach ($last as $value){
					if ($value == intval ($value)){
						$input[]=$value;
					}
				}
			}
		}
		$input[]=$_GET['new'];
	}
}
if (isset($input) && is_array($input)){
	list($folders, $img)=folders_and_images_in_path($path, $root);
	foreach($input as $value){
		list($folders, $img)=folders_and_images_in_path($path, $root);
		if ( $value > count($folders)){
			$path=$root;
			unset($input);
			break;
		}
		$path = $path.$folders[$value].'/';
		list($folders, $img)=folders_and_images_in_path($path, $root);
	}
}

if (!isset($folders) || !isset($img))
{
	list($folders, $img)=folders_and_images_in_path($path, $root);
}
if (!isset($input))
{
	$input=array();
}

echo json_encode(array ("dir" => $folders, "img" => $img, "last_valid_url_input" => json_encode($input)));
?>