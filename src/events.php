<?php

use Compucie\Congressus\ExtendedClient;
use Compucie\Database\Event\EventDatabaseManager;

require_once('../vendor/autoload.php');

header("Content-Type: application/json;");

$env = parse_ini_file("../.env", true);
$dbm = new EventDatabaseManager($env['db-event']);
$pinnedEventIds = $dbm->getCurrentlyPinnedEventIds();

$congressusClient = new ExtendedClient($env['congressus']['token']);
$upcomingEvents = $congressusClient->listUpcomingEvents();

$pinnedEventsJson = array();
$upcomingEventsJson = array();
foreach ($upcomingEvents as $event) {
    $eventJson = array();
    $eventJson['name'] = $event->getName();
    $eventJson['start'] = $event->getStart();
    in_array($event->getId(), $pinnedEventIds) ? ($pinnedEventsJson[] = $eventJson) : ($upcomingEventsJson[] = $eventJson);
}

echo json_encode(["pinned" => $pinnedEventsJson, "upcoming" => $upcomingEventsJson]);
