<?php

namespace Compucie\Scherm\Spotify;

require "{$_SERVER['DOCUMENT_ROOT']}/vendor/autoload.php";

session_start();

$env = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/.env", true);
$session = new \SpotifyWebAPI\Session(...$env['spotify']);

// protect against CSRF
$state = $session->generateState();
$_SESSION['state'] = $state;

$options = [
    'scope' => [
        'user-read-playback-state',
    ],
    'state' => $state,
];

header('Location: ' . $session->getAuthorizeUrl($options));
die();
