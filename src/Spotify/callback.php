<?php

namespace Compucie\Scherm\Spotify;

require "{$_SERVER['DOCUMENT_ROOT']}/vendor/autoload.php";

session_start();

// protect against CSRF
if ($_GET['state'] !== $_SESSION['state']) {
    die('State mismatch');
}

$env = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/.env", true);
$session = new \SpotifyWebAPI\Session(...$env['spotify']);

$session->requestAccessToken($_GET['code']);

// store the tokens but not the session object, because the session object contains the client secret
$_SESSION['accessToken'] = $session->getAccessToken();
$_SESSION['refreshToken'] = $session->getRefreshToken();

header('Location: https://scherm.isaacnewton.nl');
die();
