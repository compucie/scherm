<?php

namespace Compucie\Scherm\Spotify;

require "{$_SERVER['DOCUMENT_ROOT']}/vendor/autoload.php";

session_start();

// create session
$env = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/.env", true);
$session = new \SpotifyWebAPI\Session(...$env['spotify']);

if (isset($_SESSION['accessToken'])) {
    $session->setAccessToken($_SESSION['accessToken']);
    $session->setRefreshToken($_SESSION['refreshToken']);
} else {
    $session->refreshAccessToken($_SESSION['refreshToken']);
}

// request data from spotify
$api = new \SpotifyWebAPI\SpotifyWebAPI(options: ['auto_refresh' => true], session: $session);

$playbackInfo = $api->getMyCurrentPlaybackInfo();

$artistNames = array();
foreach ($playbackInfo->item->artists as $artist) {
    $artistNames[] = $artist->name;
}

$data = [
    "albumSrc" => $playbackInfo->item->album->images[1]->url,
    "songName" => $playbackInfo->item->name,
    "artistNames" => implode(", ", $artistNames),
    "progress_ms" => $playbackInfo->progress_ms,
    "duration_ms" => $playbackInfo->item->duration_ms,
];

// send data to front end
header("Content: application/json;");
echo json_encode($data);

// update tokens
$_SESSION['accessToken'] = $session->getAccessToken();
$_SESSION['refreshToken'] = $session->getRefreshToken();
