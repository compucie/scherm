const startTime = Date.now();
const videoTime = 10 * 60 * 1000;
const videoUrl = "sponsor/video.mp4";
const videoExists = new File([""],videoUrl);
// const videoExists = false;
console.log(videoExists);
let nextTime = startTime + videoTime;

const BIRTHDAYS_URL = "src/birthdays.php";
const EVENTS_URL = "src/events.php";
const PHOTO_URL = "src/photo.php";
const SPOTIFY_URL = "src/Spotify/app.php";

$(function () {
    spotify();
    birthdays();
    events();

    //------------------------ select folder -------------------------------------
    $("#photo").hide();
    $("#video_wrap").hide();
    $.getJSON(PHOTO_URL + '?' + decodeURIComponent(window.location.search.substring(1)), function (data) {//url parameters doorgeven
        if (data.dir.length > 1) {
            $.each(data.dir, function (key, name) {
                $("#folder_selector").append("<a type='button' href='?last=" + data.last_valid_url_input + "&new=" + key + "' class='folder_select_button btn btn-default btn-lg'>" + name + "</a >");
            })
        }
        else {
            $("#folder_selector").hide();
            $("#photo").show();
            img_urls = data.img;
            img_url_id = 0;
            next_image();
        }
    });

    //reclame banner
    $.getJSON(PHOTO_URL + "?dir=banner", function (data) {
        var reclame_urls = data.img;
        var reclame_url_id = 0;
        $("#reclame_banner").attr("src", reclame_urls[reclame_url_id]); //run directly first time
        setInterval(
            function () {
                $("#reclame_banner").attr("src", reclame_urls[reclame_url_id]); //repeat after timer
                reclame_url_id++;
                if (reclame_url_id >= reclame_urls.length) reclame_url_id = 0;
            }, 57000);//timer for loop
    });
});

function spotify() {
    $.getJSON(SPOTIFY_URL, function (data) {
        $("#spotify-album").attr("src", data.albumSrc);
        $("#spotify-song-title").html(data.songName);
        $("#spotify-artists").html(data.artistNames);
    });
    setTimeout(spotify, 5700);
}

function birthdays() {
    $.getJSON(BIRTHDAYS_URL, function (data) {
        $("#birthdays-content").empty();
        $.each(data, function (key, name) {
            console.log(name);
            $("#birthdays-content").append(name + "<br>");
        });
    });
    setTimeout(birthdays, 60 * 60 * 1000);
}

function events() {
    $.ajax({
        url: EVENTS_URL,
        success: function (events) {
            pinnedEvents(events.pinned);
            upcomingEvents(events.upcoming);
        }
    })
    setTimeout(events, 60 * 1000);
}

function pinnedEvents(pinnedEvents) {
    if (pinnedEvents.length == 0) {
        $("#pinned-events").css("display", "none");
        return;
    }

    $("#pinned-events-content").empty();
    $.each(pinnedEvents, function (i, event) {
        $("#pinned-events-content").append(createEventElement(event));
    });
}

function upcomingEvents(upcomingEvents) {
    // clear existing upcoming event elements so the list is built from scratch
    const dead = document.getElementsByClassName("upcoming");
    while (dead[0]) {
        dead[0].parentNode.removeChild(dead[0]);
    }

    // first event element contains the heading so is created outside of the loop
    // this is done so the heading is never shown without any upcoming events
    firstEvent = upcomingEvents[0];
    upcomingEvents.shift();
    if (!firstEvent) {
        return;
    }

    // this is the event without the heading
    const eventElement = createEventElement(firstEvent);
    eventElement.getElementById("event-row").classList.add("upcoming");

    // this includes the heading
    const firstEventTemplate = document.querySelector("#first-event-template");
    const firstEventElement = firstEventTemplate.content.cloneNode(true);
    firstEventElement.getElementById("first-event").append(eventElement);

    $("#info").append(firstEventElement);

    // each upcoming event after the first one
    $.each(upcomingEvents, function (i, event) {
        const eventElement = createEventElement(event);
        eventElement.getElementById("event-row").classList.add("upcoming");
        $("#info").append(eventElement);
    });
}

function createEventElement(event) {
    const eventTemplate = document.querySelector("#event-template");
    const eventElement = eventTemplate.content.cloneNode(true);
    eventElement.getElementById("event-date").textContent = (new Date(event.start.date)).toLocaleString('en-GB', { day: 'numeric', month: 'short' });
    eventElement.getElementById("event-name").textContent = event.name;
    return eventElement;
}

//------------------------ main img loop -------------------------------------
function change_img(url) {
    fade_time = 0;
    console.log(url)
    exten = url.split('.').pop().toLowerCase();
    if (exten == "jpg" || exten == "jpeg" || exten == "png" || exten == "gif") {
        $("#videosrc").attr("src", "");
        $("#video_wrap").fadeOut(fade_time);
        $("#photo").fadeOut(fade_time).attr("src", "https://kamerphoto.isaacnewton.nl/" + url).fadeIn(fade_time);
        setTimeout(next_image, 5700);		//set timeout for next image
    } else if (exten == "mp4") {
        $("#photo").attr("src", "");
        $("#photo").fadeOut(fade_time);
        $("#video_wrap").fadeOut(fade_time);
        $("#videosrc").attr("src", "https://kamerphoto.isaacnewton.nl/" + url);
        $('#video').load();
        $("#video_wrap").fadeIn(fade_time);
        $('#video').attr('autoplay', 'autoplay');
        var video = document.getElementById('video');
        $('#video').on('loadeddata', function () {
            videoDuration = $('#video').get(0).duration * 1000;
            if (videoDuration) {
                setTimeout(next_image, $('#video').get(0).duration * 1000);//set timeout for image duration
            }
            $('#video').off('loadeddata');
        });
    }
    var foldername_arr = url.split('/');
    $("#photo-album").html(foldername_arr[1].replace(/[0-9.,\/#$%\^\*;:{}=\-`~()]/g, '').replace(/_/g, ' ').replace(/uitgezocht/, ''));
    $("#photo-year").html(foldername_arr[0]);
}

//go to the next image in the array
function next_image() {
    if (nextTime <= Date.now() && videoExists) {
        change_img(videoUrl);
        nextTime = nextTime + videoTime;
    } else {
        change_img(img_urls[img_url_id]);
        img_url_id++;
        if (img_url_id >= img_urls.length) img_url_id = 0;
        //preload next image dirty way
        $("#img_preload").attr("src", "https://kamerphoto.isaacnewton.nl/" + img_urls[img_url_id]).hide();
    }
}
