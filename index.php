<!DOCTYPE html>
<html lang="nl">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="Harm van der Maas">
	<title>W.S.G. Isaac Newton</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
	<link rel="stylesheet" href="public/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="public/script.js"></script>

	<?php
	session_start();
	if (empty($_SESSION['accessToken'])) {
		header('Location: src/Spotify/auth.php');
	} else {
		echo "<script>console.log('auth successful');</script>";
	}
	?>
</head>

<aside id="left_banner">
	<svg id="left_banner_logo" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
		x="0px" y="0px" viewBox="0 0 1024 1024" style="enable-background:new 0 0 1024 1024;" xml:space="preserve">
		<path fill="#489a2b" class="st0"
			d="M858.7,437.4v340C858.7,777.4,1006.2,616.1,858.7,437.4z M584.4,579.6V397.8c9.8-49.4,74.8-37.1,89.7-36.3
			c14.9,0.8,49.6,11.9,49.6,11.9v305.7C723.6,681.5,585.1,578.7,584.4,579.6z M438.2,637v197c-9.9,49.4-54.8,31.2-54.8,31.2l-84.5-19
			V537.5C298.9,535.2,437.4,637.9,438.2,637z M584.8,745.1l137.8,99.6c-0.4-0.4-94.9,35.7-123.7,15.6
			C575.4,840.1,584.9,781.8,584.8,745.1z M165.3,435.1v340C165.3,775.1,17.8,613.8,165.3,435.1z M299.7,370.1l137.7,99.6
			c-0.1-36.6,7.8-85.6-14.8-97.7C379.7,346.7,300.1,370.5,299.7,370.1z M548.2,274c59.8-18.4,269-32.8,375.3,76
			c102.8,105.2,103.5,215.5,85.3,321.7c-18.3,106.2-90.2,178.4-196.4,233.5c-106.3,55.1-236.9,57-302.2,57.9
			c-55.6,0.7-258.4-11.1-369.2-93.6C35.3,790.8,6,715.3,6.2,585c0.1-129.6,54.3-240.8,188.1-295.1c133.7-54.3,270.2-16,270.2-16
			l2.3-7.6c0,0,22.1-65.4-88.3-124c0,0,33.4-13.1,22-81.4c0,0,121.9,29.5,141.6,171.1C545.4,255.6,548.2,274,548.2,274z" />
	</svg>

	<div id="left_banner_text">
		W.S.G. Isaac Newton
	</div>
</aside>

<body>
	<img id="img_preload" style="display: hidden">

	<!-- CONTENT -->
	<div id="container_next_to_newton_banner">

		<!-- BOTTOM AD BANNER -->
		<div id="div_reclame_banner">
			<img id="reclame_banner" src="public/NewtonBanner_MEstudentDay.png">
		</div>

		<!-- PHOTOS AREA -->
		<div class="col-xs-8" style="height: 100%;">

			<!--Album title-->
			<div id="photo-heading">
				<span id="photo-album"></span><br>
				<span id="photo-year"></span>
			</div>

			<!-- PHOTOS -->
			<div id="div_photo" class="col-xs-12">
				<!--hide photo or folder_selector in script-->
				<img id="photo">
				<div id="video_wrap" align="center" class="embed-responsive embed-responsive-16by9">
					<video id="video" autoplay muted> <!-- width="2000"  -->
						<source id="videosrc" type="video/mp4">
					</video>
				</div>
				<div id="folder_selector" style="z-index:100;"></div>
			</div>
		</div>

		<!-- RIGHT INFO BAR -->
		<div id="info">

			<!-- Spotify. Atomic element -->
			<div id="spotify" class="info-element">
				<img id="spotify-album" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Spotify_logo_without_text.svg/1024px-Spotify_logo_without_text.svg.png">
				<div id="spotify-names">
					<span id="spotify-song-title">Nothing is being played</span><br>
					<span id="spotify-artists"></span>
				</div>
			</div>

			<!-- Birthdays. Atomic element -->
			<div id="birthdays" class="info-element">
				<h1>Birthdays</h1>
				<div id="birthdays-content"></div>
			</div>

			<!-- Event row template -->
			<template id="event-template">
				<div id="event-row" class='info-element event'>
					<div id="event-date" class='col-xs-3 event-date'></div>
					<div id="event-name" class='col-xs-9 event-name'></div>
				</div>
			</template>

			<!-- Pinned events. Atomic element -->
			<div id="pinned-events" class="info-element">
				<h1>Pinned events</h1>
				<div id="pinned-events-content"></div>
			</div>

			<!-- Upcoming events. May be partially shown -->
			<template id="first-event-template">
				<div id="first-event" class="info-element event upcoming">
					<h1>Upcoming events</h1>
				</div>
			</template>

		</div>
	</div>
</body>

</html>